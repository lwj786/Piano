#!/usr/bin/env python3

# use passive buzzer and GPIO(PWM output) to play tune

import RPi.GPIO as GPIO
import time

EXPLAIN = \
'''-h, --help
--output=<num> set up GPIO pin
--bpm=<num> beat per minute，set the speed of play, default as 120
--tune=<musical notation | file> set the musical notation or file containing musical notation
\tthe rule of musical notation: like numbered musical notation but only support some representation
\tmusical notes: 1 2 3 4 5 6 7, '#', 'b' can be used and use 0 for musical rest
\toctave: use '.' in left of note to raise and in right of it to lower, ".1", "2.."
\tnote length:
\t            dash '-' lengthen it, "1 -";
\t            underline '_' halves the length, "2_";
\t            asterisk '*'  increases its length by half, "3*"
\trelative position: [.][#b][0-7][.][_][*] [-]
\tuse blankspace or '\\n' to separate musical notes  and bar lines can be used to make it good to read
\tothers like key signature or time signature is not supported now,
\texample: 1 1 5 5 | 6 6 5 - | 4 4 3 3 | 2 2 1 - ||
'''

# refer to https://zh.wikipedia.org/wiki/%E9%9F%B3%E9%AB%98#.E9.9F.B3.E9.AB.98.E9.A0.BB.E7.8E.87.E8.A1.A8
pitch2frequency = {
    'C': [16.352, 32.703, 65.406, 130.81, 261.63, 523.25, 1046.5, 2093.0, 4186.0, 8372.0],
    'C#/Db': [17.324, 34.648, 69.296, 138.59, 277.18, 554.37, 1108.7, 2217.5, 4434.9, 8869.8],
    'D': [18.354, 36.708, 73.416, 146.83, 293.66, 587.33, 1174.7, 2349.3, 4698.6, 9397.3],
    'D#/Eb': [19.445, 38.891, 77.782, 155.56, 311.13, 622.25, 1244.5, 2489.0, 4978.0, 9956.1],
    'E': [20.602, 41.203, 82.407, 164.81, 329.63, 659.26, 1318.5, 2637.0, 5274.0, 10548],
    'F': [21.827, 43.654, 87.307, 174.61, 349.23, 698.46, 1396.9, 2793.8, 5587.7, 11175],
    'F#/Gb': [23.125, 46.249, 92.499, 185.00, 369.99, 739.99, 1480.0, 2960.0, 5919.9, 11840],
    'G': [24.500, 48.999, 97.999, 196.00, 392.00, 783.99, 1568.0, 3136.0, 6271.9, 12544],
    'G#/Ab': [25.957, 51.913, 103.83, 207.65, 415.30, 830.61, 1661.2, 3322.4, 6644.9, 13290],
    'A': [27.500, 55.000, 110.00, 220.00, 440.00, 880.00, 1760.0, 3520.0, 7040.0, 14080],
    'A#/Bb': [29.135, 58.270, 116.54, 233.08, 466.16, 932.33, 1864.7, 3729.3, 7458.6, 14917],
    'B': [30.868, 61.735, 123.47, 246.94, 493.88, 987.77, 1975.5, 3951.1, 7902.1,  15804]
}

''' func: paly()
    '''
def play(stream, bpm, channel):
    time_of_beat = 60 / bpm

    stream = [[f, l * time_of_beat] for [f, l] in stream]

    tune = GPIO.PWM(channel, 1)

    tune.start(50)
    for [f, l] in stream:
        tune.ChangeFrequency(f)
        time.sleep(l)
    tune.stop()

''' func: tune2stream()
    convert musical notes to frequency (and note length) stream '''
def tune2stream(notes):
    note2pitch = {
        '1': 'C', '#1': 'C#/Db', 'b2': 'C#/Db',
        '2': 'D', '#2': 'D#/Eb', 'b3': 'D#/Eb',
        '3': 'E',
        '4': 'F', '#4': 'F#/Gb', 'b5': 'F#/Gb',
        '5': 'G', '#5': 'G#/Ab', 'b6': 'G#/Ab',
        '6': 'A', '#6': 'A#/Bb', 'b7': 'A#/Bb',
        '7': 'B'
    }

    octave, underlines, dots_after = 0, 0, 0

    frequency, note_length = 0, 1
    stream = []

    for note in notes:
        if not len(note):
            continue

        if note == "-":
            stream[-1][1] += 1

        if note[0] == '.':
            octave = note.count('.')
        else:
            octave = - note.count('.')
        note = note.replace('.', '')

        underlines = note.count('_')
        note = note.replace('_', '')
        dots_after= note.count('*')
        note = note.replace('*', '')

        try:
            if note == '0':
                frequency = 1
            else:
                frequency = pitch2frequency[note2pitch[note]][4 + octave]
        except:
            continue

        note_length = 1/pow(2, underlines) * (2 - 1/pow(2, dots_after))

        stream.append([frequency, note_length])

        octave, underlines, dots_after = 0, 0, 0

    return stream


''' func: action()
    '''
def action(channel, _name, params):
    bpm = 120
    notation = "1 2 3 4 5 6 7 .1"

    for parameter in params:
        if parameter[:len("--bpm=")] == "--bpm=":
            try:
                bpm = int(parameter[len("--bpm="):])
            except:
                bpm = 120
        elif parameter[:len("--tune=")] == "--tune=":
            try:
                with open(parameter[len("--tune="):]) as f:
                    notation = f.read()
            except:
                notation = parameter[len("--tune="):]

    notes = notation.replace('\n', ' ').replace('|', '').split(' ')

    channel = channel[0] if type(channel) == list else channel
    play(tune2stream(notes), bpm, channel)

''' func: main()
    set up output channel '''
def main(argv):
    GENERAL_GPIO_BCM_CODE = \
        [4, 17, 18, 27, 22, 23, 24, 25, 5, 6, 12, 13, 19, 16, 26, 20, 21]
    channel = 0

    for arg in argv:
        if arg[:len("--output=")] == "--output=":
            try:
                channel = int(arg[len("--output="):])
            except:
                continue
        elif arg == "--help" or arg == "-h":
            print(EXPLAIN)
            exit()

    if not (channel and channel in GENERAL_GPIO_BCM_CODE):
        exit()

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(channel, GPIO.OUT)

    action(channel, "Piano", argv)

    GPIO.cleanup()

if __name__  == "__main__":
    from sys import argv
    main(argv)